#include "nepomuktelepathylogservice.h"

#include "nepomuk-storage.h"
#include "controller.h"

#include <TelepathyQt4/AccountManager>
#include <TelepathyQt4/PendingOperation>
#include <TelepathyQt4/PendingReady>
#include <TelepathyLoggerQt4/LogManager>

#include <Nepomuk/Vocabulary/NCO>
#include <Nepomuk/Vocabulary/NMO>
#include <Nepomuk/Resource>
#include <Nepomuk/Utils/SimpleResourceModel>

#include <nepomuk/simpleresource.h>
#include <nepomuk/simpleresourcegraph.h>
#include <nepomuk/datamanagement.h>

#include <KDebug>
#include <QUrl>


using namespace Nepomuk::Vocabulary;

TelepathyLogService::TelepathyLogService(QObject* parent, const QVariantList&)
    : Nepomuk::Service()
{
    // Initialise Telepathy.
    Tp::registerTypes();
    Controller* controller = new Controller( new NepomukStorage() );
}

TelepathyLogService::~TelepathyLogService()
{
}

#include "nepomuktelepathylogservice.moc"