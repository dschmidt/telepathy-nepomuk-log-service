/*
 * This file is part of telepathy-nepomuk-service
 *
 * Copyright (C) 2009-2011 Collabora Ltd. <info@collabora.co.uk>
 *   @author George Goldberg <george.goldberg@collabora.co.uk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "controller.h"
#include "account.h"
#include "abstract-storage.h"
#include "nepomuk-storage.h"
#include "contact.h"

#include <KDebug>

#include <TelepathyQt4/Types>
#include <TelepathyQt4/Account>
#include <TelepathyQt4/PendingReady>

#include <TelepathyLoggerQt4/Init>
#include <TelepathyLoggerQt4/LogManager>
#include <TelepathyLoggerQt4/Entity>
#include <TelepathyLoggerQt4/PendingDates>
#include <TelepathyLoggerQt4/PendingEvents>
#include <TelepathyLoggerQt4/Event>
#include <TelepathyLoggerQt4/TextEvent>
#include <TelepathyLoggerQt4/CallEvent>

#include <glib-2.0/glib-object.h>
#include <QGlib/Init>


#include <Nepomuk/Vocabulary/NCO>
#include <Nepomuk/Vocabulary/NMO>
#include <Nepomuk/Resource>
#include <Nepomuk/Utils/SimpleResourceModel>

#include <nepomuk/simpleresource.h>
#include <nepomuk/simpleresourcegraph.h>
#include <nepomuk/datamanagement.h>
#include <TelepathyQt4/AccountManager>
#include <TelepathyQt4/PendingOperation>


Controller::Controller(NepomukStorage *storage, QObject *parent)
 : QObject(parent),
   m_storage(storage)
{
    g_type_init();
    QGlib::init();
    Tpl::init();

    // Become the parent of the Storage class.
    m_storage->setParent(this);

    // We must wait for the storage to be initialised before anything else happens.
    connect(m_storage, SIGNAL(initialised(bool)), SLOT(onStorageInitialised(bool)));
}


Controller::~Controller()
{
    kDebug();
}


void Controller::onAccountManagerReady(Tp::PendingOperation* po)
{
    if(po->isError())
    {
        Q_ASSERT("Error in AccountManager");
        return;
    }

     m_logManager = Tpl::LogManager::instance();
    if (m_logManager.isNull()) {
        qWarning() << "LogManager not found";
        return;
    }
    m_logManager->setAccountManagerPtr( m_accountManager );

    connect(m_storage, SIGNAL(contactCached(ContactIdentifier)), SLOT(onContactCached(ContactIdentifier)));
    Q_FOREACH(const ContactIdentifier& contact, m_storage->contacts())
    {
        kDebug() << "call onContact Cached";
        onContactCached( contact );
    }


}

void Controller::onStorageInitialised(bool success)
{
    kDebug() << "nepomuk initialised all contacts, starting to get logs" << m_storage->contacts().count();

    if (!success) {
        kDebug() << "init failed";
        emit storageInitialisationFailed();
        return;
    }

    kDebug() << "init succeeded";
    m_accountManager = Tp::AccountManagerPtr()->create();
    connect(m_accountManager->becomeReady(Tp::Features() << Tp::AccountManager::FeatureCore),
            SIGNAL(finished(Tp::PendingOperation*)),
            SLOT(onAccountManagerReady(Tp::PendingOperation*)));
}

void Controller::onDatesFinished(Tpl::PendingOperation *po)
{
    Tpl::PendingDates *pd = (Tpl::PendingDates*) po;

    if (pd->isError()) {
        qWarning() << "error in PendingDates";
        Q_ASSERT(false);
        return;
    }

    QList<QDate> dates = pd->dates();

    //kDebug() << pd->account()->uniqueIdentifier() << pd->entity()->identifier() << dates;

    Q_FOREACH(const QDate& date, dates)
    {
        kDebug() << "requesting log for" << pd->account()->uniqueIdentifier() << pd->entity()->identifier() << "on" << date;
        Tpl::PendingEvents* events = m_logManager->queryEvents( pd->account(), pd->entity(), Tpl::EventTypeMaskAny, date);

        connect(events, SIGNAL(finished(Tpl::PendingOperation*)), SLOT(onEventsFinished(Tpl::PendingOperation*)));
    }

}

void Controller::onEventsFinished(Tpl::PendingOperation *po)
{
    Tpl::PendingEvents *pe = (Tpl::PendingEvents*) po;

    if (pe->isError()) {
        qWarning() << "error in PendingEvents" << pe->errorMessage();
        ::exit(-1);
        return;
    }

    QList<Tpl::EventPtr> events = pe->events();

    kDebug() << "save logs now";
    Q_FOREACH(const Tpl::EventPtr& event, events)
    {
        //kDebug() << "uniqueIdentifier" << (event->account().isNull() ? QString("ACCOUNT NULL -.-") : event->account()->uniqueIdentifier());
        //kDebug() << "normalizedName" << (event->account().isNull() ? QString("ACCOUNT NULL -.-") : event->account()->normalizedName());

        Tpl::TextEventPtr textEvent = event.dynamicCast<Tpl::TextEvent>();
        Tpl::CallEventPtr callEvent = event.dynamicCast<Tpl::CallEvent>();

        Nepomuk::SimpleResourceGraph g;

        Nepomuk::SimpleResource resource;


        SimpleResource receivedDateResource;
        resource.setProperty(Nepomuk::Vocabulary::NMO::receivedDate(), );


        Nepomuk::SimpleResource senderResource( imAccountForEvent(event, EventPartyTypeSender).resourceUri() );
        senderResource.addType( Nepomuk::Vocabulary::NCO::IMAccount() );
        senderResource.addType( Nepomuk::Vocabulary::NCO::ContactMedium() );
        g << senderResource;
        resource.setProperty(Nepomuk::Vocabulary::NMO::messageSender(), senderResource );


        Nepomuk::SimpleResource receiverResource( imAccountForEvent(event, EventPartyTypeReceiver).resourceUri() );
        receiverResource.addType( Nepomuk::Vocabulary::NCO::IMAccount() );
        receiverResource.addType( Nepomuk::Vocabulary::NCO::ContactMedium() );
        resource.setProperty(Nepomuk::Vocabulary::NMO::messageRecipient(), receiverResource );
        g << receiverResource;


        if(!textEvent.isNull())
        {
            kDebug()    << event->timestamp()
                        << "from" << event->sender()->identifier()
                        << "to" << event->receiver()->identifier()
                        << textEvent->message();
            kDebug();

            resource.addType(Nepomuk::Vocabulary::NMO::IMMessage());
            resource.setProperty(Nepomuk::Vocabulary::NMO::plainTextMessageContent(), textEvent->message());
            resource.setProperty(Nepomuk::Vocabulary::NMO::isRead(), true);
        }

        if(!callEvent.isNull())
        {
            //kDebug() << callEvent->
            callEvent->duration();
            //callEvent->
        }

        g << resource;

        kDebug() << g;
        KJob* storeJob = Nepomuk::storeResources(g);
    }
}


Nepomuk::IMAccount Controller::imAccountForEvent(const Tpl::EventPtr& event, EventPartyType partyType)
{
    Tpl::EntityPtr entity;
    QString identifier;
    if(partyType == EventPartyTypeSender)
    {
        entity = event->sender();
        identifier = entity->identifier();
    }
    else
    {
        entity = event->receiver();
        identifier = entity->identifier();

        QString shittyWrongNameOfMyself = QString("%1/%2/%3")
                                            .arg(event->account()->cmName())
                                            .arg(event->account()->protocolName())
                                            .arg(identifier);

        if(shittyWrongNameOfMyself == event->account()->uniqueIdentifier())
        {
            kDebug() << "Yay, telepathy-logger tells us our uniqueIdentifier instead of a proper name, this should not happen :-(";
            kDebug() << "Doing bloody hack now :-(";
            identifier = event->account()->normalizedName();
        }
    }


    // make sure no wrong jabber account identifier slipped through in gabble
    // don't know any way for other cms/protocols
    Q_ASSERT(!identifier.contains("_40"));

    ContactIdentifier contact = ContactIdentifier( event->accountPath(), identifier );

    Nepomuk::IMAccount imAccount = m_storage->contactImAccount( contact );
    return imAccount;
}


void Controller::onContactCached(const ContactIdentifier& contact)
{
    kDebug() << "GET LOGS FOR " << contact.accountId() << contact.contactId();


    Tp::AccountPtr account = Tp::Account::create(
                            QString::fromAscii(TELEPATHY_ACCOUNT_MANAGER_BUS_NAME),
                            contact.accountId()
                        );

    Tpl::EntityPtr entity = Tpl::Entity::create(contact.contactId().toStdString().c_str(),
                                                Tpl::EntityTypeContact,
                                                NULL,
                                                NULL);

    Tpl::PendingDates* dates = m_logManager->queryDates( account, entity, Tpl::EventTypeMaskText);

    m_contactsPendingDates.insert( new ContactIdentifier( account->uniqueIdentifier(), entity->identifier() ), dates );
    connect(dates, SIGNAL(finished(Tpl::PendingOperation*)), SLOT(onDatesFinished(Tpl::PendingOperation*)));
}


#include "controller.moc"

