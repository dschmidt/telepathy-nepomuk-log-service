#ifndef NEPOMUKTELEPATHYLOGSERVICE_H
#define NEPOMUKTELEPATHYLOGSERVICE_H

#include "Nepomuk/Service"

#include <KPluginFactory>
#include <KPluginLoader>

namespace Tp {
    class PendingOperation;
}

class TelepathyLogService : public Nepomuk::Service
{
   Q_OBJECT

public:
    TelepathyLogService(QObject* parent, const QVariantList&);
    virtual ~TelepathyLogService();
};

NEPOMUK_EXPORT_SERVICE(TelepathyLogService, "nepomuktelepathylogservice" );

#endif // NEPOMUKTELEPATHYLOGSERVICE_H
